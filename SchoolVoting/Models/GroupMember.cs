﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class GroupMember
    {
        [Key]
        public int GroupMemberId { get; set; }
        public int GrupoId { get; set; }
        public int UtlizadorId { get; set; }

        public virtual Grupo Grupo { get; set; }
        public virtual Utilizador Utilizador { get; set; }

    }
}