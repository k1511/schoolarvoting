﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class GrupoDetailsView
    {

        public int GrupoId { get; set; }
        public string Descricao { get; set; }
        public List<GroupMember> Members { get; set; }

    }
}