﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class Utilizador
    {
        [Key]
        public int UtlizadorId { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(100, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 7)]
        [Display(Name = "E-Mail")]
        [DataType(DataType.EmailAddress)]
        [Index("EMailIndex", IsUnique = true)]
        public string EMail { get; set; }


        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(50, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 2)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(50, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 2)]
        [Display(Name = "Apelido")]
        public string Apelido { get; set; }

        [Display(Name = "Nome Completo")]
        public string FullName { get {return string.Format("{0} {1}",Nome,Apelido) ;} }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(15, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 7)]
        public string Telefone { get; set; }



        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(100, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 4)]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Display(Name = "Nível")]
        public string Nivel { get; set; }
        public string Grupo { get; set; }


        [DataType(DataType.ImageUrl)]
        [StringLength(300, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 4)]
        public string Foto { get; set; }

        public virtual ICollection<GroupMember> GroupMembers { get; set; }

    }
}