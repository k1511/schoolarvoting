﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class AddMemberView
    {
        public int GrupoId { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        public int UtlizadorId { get; set; }

    }
}