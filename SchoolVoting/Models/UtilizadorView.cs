﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class UtilizadorView
    {
        public int UtlizadorId { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(100, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 7)]
        [Display(Name = "E-Mail")]
        [DataType(DataType.EmailAddress)]
        public string EMail { get; set; }


        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(50, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 2)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(50, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 2)]
        [Display(Name = "Apelido")]
        public string Apelido { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(15, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 7)]
        public string Telefone { get; set; }



        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(100, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 4)]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Display(Name = "Nível")]
        public string Nivel { get; set; }
        public string Grupo { get; set; }

       public HttpPostedFileBase Foto { get; set; }
    }
}