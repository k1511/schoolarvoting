﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class VotingView
    {
      
        public int IdVotante { get; set; }


        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(100, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 5)]
        [Display(Name = "Descrição voto")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Classe")]
        public int StateId { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Data início")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DataInicio { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Hora início")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime HoraInicio { get; set; }


        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Data Fim")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DataFim { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Hora final")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime HoraFim { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "É para todos usuários?")]
        public bool EParaTodosUsuarios { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "É voto em branco hábil?")]
        public bool EVotoHabilEmBranco { get; set; }

    }
}