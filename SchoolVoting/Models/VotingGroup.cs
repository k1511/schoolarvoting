﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class VotingGroup
    {
        [Key]
        public int VotingGroupId { get; set; }
        public int IdVotante { get; set; }
        public int GrupoId { get; set; }

        public virtual Votante Votante { get; set; }
        public virtual Grupo Grupo { get; set; }


    }
}