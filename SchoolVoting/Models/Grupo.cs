﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class Grupo
    {
        [Key]
        //Id Grupo
        public int GrupoId { get; set; }

        //Campo Descrição
        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(50, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 3)]
        public string Descricao { get; set; }

        public virtual ICollection<GroupMember> GroupMembers { get; set; }
        public virtual ICollection<VotingGroup> VotingGroups { get; set; }



    }
}