﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class Classes
    {
        [Key]
        public int StateId { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(50, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 3)]
        [Display(Name = "Classe")]
        public string Descricao { get; set; }

        public virtual ICollection<Classes> Votos { get; set; }
    }
}