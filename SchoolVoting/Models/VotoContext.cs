﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class VotoContext : DbContext
    {
        public VotoContext() : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
        public DbSet<Classes> Classes { get; set; }

        public DbSet<Grupo> Grupoes { get; set; }

        public DbSet<Votante> Votantes { get; set; }

        public DbSet<Utilizador> Utilizadors { get; set; }

        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<VotingGroup> VotingGroups { get; set; }
    }
}