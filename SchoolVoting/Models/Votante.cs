﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolVoting.Models
{
    public class Votante
    {
        [Key]
        public int IdVotante { get; set; }


        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [StringLength(100, ErrorMessage = "O campo {0} deve conter no máx. {1} e mín. {2} caracters",
            MinimumLength = 5)]
        [Display(Name = "Descrição voto")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Classe")]
        public int StateId { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Data hora início")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime DataHoraInicio { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "Data hora fim")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime DataHoraFim { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "É para todos usuários?")]
        public bool EParaTodosUsuarios { get; set; }

        [Required(ErrorMessage = "Campo {0} obrigatório!")]
        [Display(Name = "É voto em branco hábil?")]
        public bool EVotoHabilEmBranco { get; set; }

        [Display(Name = "Quantidade votos")]
        public int QdeVotos { get; set; }

        [Display(Name = "Quantidade votos em branco")]
        public int QdeVotosBranco { get; set; }

        [Display(Name = "Vencedor")]
        public int CandidatoIdVencedor { get; set; }


        public virtual Classes State { get; set; }

        public virtual ICollection<VotingGroup> VotingGroups { get; set; }

    }
}