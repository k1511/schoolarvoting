﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SchoolVoting.Startup))]
namespace SchoolVoting
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
