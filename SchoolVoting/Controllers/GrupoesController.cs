﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolVoting.Models;

namespace SchoolVoting.Controllers
{
    [Authorize]
    public class GrupoesController : Controller
    {
        private VotoContext db = new VotoContext();

        [HttpGet]
        public ActionResult DeleteMember(int id)
        {
            var member = db.GroupMembers.Find(id);
            if (member != null)
            {
                db.GroupMembers.Remove(member);
                db.SaveChanges(); 
            }
            return RedirectToAction(string.Format("Details/{0}", member.GrupoId));
        }

        [HttpPost]
        public ActionResult AddMember(AddMemberView view)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.UtlizadorId = new SelectList(db.Utilizadors
                    .OrderBy(u => u.Nome)
                    .ThenBy(u => u.Apelido), "UtlizadorId", "FullName");
                return View(view);
            }
            var member = db.GroupMembers
                .Where(gm => gm.GrupoId == view.GrupoId && gm.UtlizadorId == view.UtlizadorId)
                .FirstOrDefault();
            if (member != null)
            {
                ViewBag.Error = "O membro já se vincula num grupo";
                return View(view);  
            }

            member = new GroupMember
            {
                GrupoId = view.GrupoId,
                UtlizadorId=view.UtlizadorId,
            };
            db.GroupMembers.Add(member);
            db.SaveChanges();

            return RedirectToAction(string.Format("Details/{0}",view.GrupoId));
        }

        [HttpGet]
        public ActionResult AddMember(int grupoId)
        {
            ViewBag.UtlizadorId = new SelectList(db.Utilizadors
                .OrderBy(u => u.Nome)
                .ThenBy(u => u.Apelido), "UtlizadorId", "FullName");
            var view = new AddMemberView
            {
                GrupoId = grupoId,
            };
            return View(view);
        }
        
        // GET: Grupoes
        public ActionResult Index()
        {
            return View(db.Grupoes.ToList());
        }

        // GET: Grupoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupo grupo = db.Grupoes.Find(id);
            if (grupo == null)
            {
                return HttpNotFound();
            }

            var view = new GrupoDetailsView
            {
                GrupoId = grupo.GrupoId,
                Descricao = grupo.Descricao,
                Members = grupo.GroupMembers.ToList(),
            };
            return View(view);
        }

        // GET: Grupoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Grupoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GrupoId,Descricao")] Grupo grupo)
        {
            if (ModelState.IsValid)
            {
                db.Grupoes.Add(grupo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(grupo);
        }

        // GET: Grupoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupo grupo = db.Grupoes.Find(id);
            if (grupo == null)
            {
                return HttpNotFound();
            }
            return View(grupo);
        }

        // POST: Grupoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GrupoId,Descricao")] Grupo grupo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grupo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(grupo);
        }

        // GET: Grupoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupo grupo = db.Grupoes.Find(id);
            if (grupo == null)
            {
                return HttpNotFound();
            }
            return View(grupo);
        }

        // POST: Grupoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Grupo grupo = db.Grupoes.Find(id);
            db.Grupoes.Remove(grupo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
