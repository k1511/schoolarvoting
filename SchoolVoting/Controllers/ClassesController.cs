﻿using SchoolVoting.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SchoolVoting.Controllers
{
    [Authorize]
    public class ClassesController : Controller
    {
        private VotoContext db = new VotoContext();
        // GET: Classes
        public ActionResult Index()
        {
            return View(db.Classes.ToList());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Classes state)
        {
            if (!ModelState.IsValid)
            {
                return View(state);
            }

            db.Classes.Add(state);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var state = db.Classes.Find(id);

            if (state == null)
            {
                return HttpNotFound();
            }
            return View(state);
        }

        [HttpPost]
        public ActionResult Edit(Classes state)
        {
            if (!ModelState.IsValid)
            {
                return View(state);
            }
            db.Entry(state).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var state = db.Classes.Find(id);

            if (state == null)
            {
                return HttpNotFound();
            }
            return View(state);
        }


        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var state = db.Classes.Find(id);

            if (state == null)
            {
                return HttpNotFound();
            }
            return View(state);
        }


        [HttpPost]
        public ActionResult Delete(int id, Classes state)
        {

            state = db.Classes.Find(id);

            if (state == null)
            {
                return HttpNotFound();
            }
            db.Classes.Remove(state);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                if (ex.InnerException !=null &&
                    ex.InnerException.InnerException!=null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ViewBag.Error="Não pode eliminar esse registo porque está relacionado com outra tabela";
                }
                else
                {
                    ViewBag.Error = ex.Message;
                }
                return View(state);                
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}