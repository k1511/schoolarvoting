﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolVoting.Models;

namespace SchoolVoting.Controllers
{
    [Authorize]
    public class VotantesController : Controller
    {
        private VotoContext db = new VotoContext();

        public  ActionResult AddGroup(int id)
        {
            ViewBag.GrupoId = new SelectList(
                db.Grupoes.OrderBy(g=>g.Descricao),
                "GrupoId", "Descricao"
                );

            var view = new AddGroupView
            {
                IdVotante = id,
            };

            return View(view);
        }
        [HttpPost]
        public ActionResult AddGroup(AddGroupView view)
        {
            if (ModelState.IsValid)
            {
                var votingGroup = db.VotingGroups
                    .Where(vg => vg.IdVotante == view.IdVotante &&
                    vg.GrupoId == view.GrupoId)
                    .FirstOrDefault();
                if (votingGroup != null)
                {
                    ViewBag.Error = "O grupo já pertence ao voto";
                    ViewBag.GrupoId = new SelectList(
                        db.Grupoes.OrderBy(g => g.Descricao),
                        "GrupoId", "Descricao"
                        );
                    return View(view);
                }

                votingGroup = new VotingGroup
                {
                    GrupoId = view.GrupoId,
                    IdVotante=view.IdVotante, 
                };
                db.VotingGroups.Add(votingGroup);
                db.SaveChanges();
                return RedirectToAction(string.Format("Details/{0}", view.IdVotante));
            }
            ViewBag.GrupoId = new SelectList(
                db.Grupoes.OrderBy(g => g.Descricao),
                "GrupoId", "Descricao"
                );
            return View(view);
        }

        // GET: Votantes
        public ActionResult Index()
        {
            var votantes = db.Votantes.Include(v => v.State);
            return View(votantes.ToList());
        }

        // GET: Votantes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var votante = db.Votantes.Find(id);
            if (votante == null)
            {
                return HttpNotFound();
            }
            return View(votante);
        }

        // GET: Votantes/Create
        public ActionResult Create()
        {
            ViewBag.StateId = new SelectList(db.Classes, "StateId", "Descricao");

            var view = new VotingView
            {
                DataInicio = DateTime.Now,
                DataFim=DateTime.Now,
            };

            return View(view);
        }

        // POST: Votantes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VotingView view)
        {
            if (ModelState.IsValid)
            {
                var votante = new Votante
                {
                    DataHoraFim = view.DataFim
                                      .AddHours(view.HoraFim.Hour)
                                      .AddMinutes(view.HoraFim.Minute),
                    DataHoraInicio= view.DataInicio
                                        .AddHours(view.HoraInicio.Hour)
                                        .AddMinutes(view.HoraInicio.Minute),
                    Descricao=view.Descricao,
                    EVotoHabilEmBranco = view.EVotoHabilEmBranco,
                    EParaTodosUsuarios = view.EParaTodosUsuarios,
                    Remarks = view.Remarks,
                    StateId=view.StateId,
                    IdVotante=view.IdVotante,
                };

                db.Votantes.Add(votante);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StateId = new SelectList(db.Classes, "StateId", "Descricao", view.StateId);
            return View(view);
        }

        // GET: Votantes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var votante = db.Votantes.Find(id);

            if (votante == null)
            {
                return HttpNotFound();
            }

            var view = new VotingView 
            {
                DataFim = votante.DataHoraFim,
                DataInicio = votante.DataHoraInicio,
                Descricao = votante.Descricao,
                EVotoHabilEmBranco = votante.EVotoHabilEmBranco,
                EParaTodosUsuarios = votante.EParaTodosUsuarios,
                Remarks = votante.Remarks,
                StateId = votante.StateId,
                HoraFim = votante.DataHoraFim,
                HoraInicio = votante.DataHoraInicio,
                IdVotante = votante.IdVotante,
            };

            ViewBag.StateId = new SelectList(db.Classes, "StateId", "Descricao", votante.StateId);
            return View(view);
        }

        // POST: Votantes/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VotingView view)
        {
            if (ModelState.IsValid)
            {
                var votante = new Votante
                {
                    DataHoraFim = view.DataFim
                                      .AddHours(view.HoraFim.Hour)
                                      .AddMinutes(view.HoraFim.Minute),
                    DataHoraInicio = view.DataInicio
                                        .AddHours(view.HoraInicio.Hour)
                                        .AddMinutes(view.HoraInicio.Minute),
                    Descricao = view.Descricao,
                    EVotoHabilEmBranco = view.EVotoHabilEmBranco,
                    EParaTodosUsuarios = view.EParaTodosUsuarios,
                    Remarks = view.Remarks,
                    StateId = view.StateId,
                    IdVotante=view.IdVotante,

                };

                db.Entry(votante).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StateId = new SelectList(db.Classes, "StateId", "Descricao", view.StateId);
            return View(view);
        }

        // GET: Votantes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Votante votante = db.Votantes.Find(id);
            if (votante == null)
            {
                return HttpNotFound();
            }
            return View(votante);
        }

        // POST: Votantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Votante votante = db.Votantes.Find(id);
            db.Votantes.Remove(votante);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
