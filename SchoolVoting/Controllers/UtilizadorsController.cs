﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolVoting.Models;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace SchoolVoting.Controllers
{
    [Authorize]
    public class UtilizadorsController : Controller
    {
        private VotoContext db = new VotoContext();

        // GET: Utilizadors
        public ActionResult Index()
        {
            return View(db.Utilizadors.ToList());
        }

        // GET: Utilizadors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utilizador utilizador = db.Utilizadors.Find(id);
            if (utilizador == null)
            {
                return HttpNotFound();
            }
            return View(utilizador);
        }

        // GET: Utilizadors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Utilizadors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UtilizadorView utilizadorView)
        {
            if (!ModelState.IsValid)
            {
                return View(utilizadorView);
            }

            string cam = string.Empty;
            string pic = string.Empty;
            if (utilizadorView.Foto != null)
            {
                pic = Path.GetFileName(utilizadorView.Foto.FileName);
                cam = Path.Combine(Server.MapPath("~/Content/Fotos"),pic);
                utilizadorView.Foto.SaveAs(cam);
                using (MemoryStream ms = new MemoryStream())
                {
                    utilizadorView.Foto.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
            }

            var utilizador = new Utilizador
            {
                EMail = utilizadorView.EMail,
                Nome = utilizadorView.Nome,
                Apelido = utilizadorView.Apelido,
                Telefone = utilizadorView.Telefone,
                Endereco = utilizadorView.Endereco,
                Nivel = utilizadorView.Nivel,
                Grupo = utilizadorView.Grupo,
                Foto = pic == string.Empty ? string.Empty : string.Format("~/Content/Fotos/{0}",pic)
            };

            db.Utilizadors.Add(utilizador);
            try
            {
                db.SaveChanges();
                this.CreateASPuser(utilizadorView);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && 
                    ex.InnerException.InnerException != null && 
                    ex.InnerException.InnerException.Message.Contains("EMailIndex"))
                {
                    ViewBag.Error = "Esse Email está sendo utilizado por outro!!";
                }
                else
                {
                    ViewBag.Error = ex.Message;
                }
                return View(utilizadorView);
           }            
            return RedirectToAction("Index");            
        }

        private void CreateASPuser(UtilizadorView utilizadorView)
        {
            var utilizadorContext = new ApplicationDbContext();
            var utilizadorManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(utilizadorContext));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(utilizadorContext));

            string roleName = "Utilizador";

            if (!roleManager.RoleExists(roleName))
            {
                roleManager.Create(new IdentityRole(roleName));
            }

            //Create the ASP.NET user
            var userASP = new ApplicationUser
            {
                UserName = utilizadorView.EMail,
                Email = utilizadorView.EMail,
                PhoneNumber=utilizadorView.Telefone,
            };

            utilizadorManager.Create(userASP, userASP.UserName);

            //Add user to role
            userASP = utilizadorManager.FindByName(utilizadorView.EMail);
            utilizadorManager.AddToRole(userASP.Id, "Utilizador"); 
        }

        // GET: Utilizadors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var utilizador = db.Utilizadors.Find(id);
            if (utilizador == null)
            {
                return HttpNotFound();
            }
            var utilizadorView = new UtilizadorView
            {
                EMail = utilizador.EMail,
                Nome = utilizador.Nome,
                Apelido = utilizador.Apelido,
                Telefone = utilizador.Telefone,
                Endereco = utilizador.Endereco,
                Nivel = utilizador.Nivel,
                Grupo = utilizador.Grupo,
                UtlizadorId=utilizador.UtlizadorId,
                //Foto = pic == string.Empty ? string.Empty : string.Format("~/Content/Fotos/{0}", pic)

            };
            return View(utilizadorView);

        }

        // POST: Utilizadors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UtilizadorView utilizadorView)
        {
            if (!ModelState.IsValid)
            {
                return View(utilizadorView);
            }

            string cam = string.Empty;
            string pic = string.Empty;
            if (utilizadorView.Foto != null)
            {
                pic = Path.GetFileName(utilizadorView.Foto.FileName);
                cam = Path.Combine(Server.MapPath("~/Content/Fotos"), pic);
                utilizadorView.Foto.SaveAs(cam);
                using (MemoryStream ms = new MemoryStream())
                {
                    utilizadorView.Foto.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
            }

            var utilizador = db.Utilizadors.Find(utilizadorView.UtlizadorId);

            utilizador.Nome = utilizadorView.Nome;
            utilizador.Apelido = utilizadorView.Apelido;
            utilizador.Telefone = utilizadorView.Telefone;
            utilizador.Endereco = utilizadorView.Endereco;
            utilizador.Nivel = utilizadorView.Nivel;
            utilizador.Grupo = utilizadorView.Grupo;

            if (!string.IsNullOrEmpty(pic))
            {
                utilizador.Foto = string.Format("~/Content/Fotos/{0}", pic);
            }


            db.Entry(utilizador).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Utilizadors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utilizador utilizador = db.Utilizadors.Find(id);
            if (utilizador == null)
            {
                return HttpNotFound();
            }
            return View(utilizador);
        }

        // POST: Utilizadors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Utilizador utilizador = db.Utilizadors.Find(id);
            db.Utilizadors.Remove(utilizador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
